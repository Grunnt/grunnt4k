# 4k games #

This repository contains 3 tiny Java applet games (of 4KB or less) and a wrapper launcher app to launch the games without the need for a browser or a browser plugin.

These games were created for the [Java4K Game Programming Competition](http://www.java4k.com/).

The games:
- Galactic Conquest 4K
- Farmer John and the birds 4K
- Wizzy 4K

## Code and build chain ##
The aim of this contest was to create as impressive a game as possible that fit within a 4k JAR file, including dependencies. So it was by no means a contest who writes the most elegant code: size was all that matters. This is reflected in the build chain, which had as its only aim to reduce the size of the resulting JAR file:
![Build chain](https://bytebucket.org/Grunnt/grunnt4k/raw/4b6e11e79eae6d476cc40ce2cf09c8141b042216/java4k_buildchain.png)