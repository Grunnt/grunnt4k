import java.applet.Applet;
import java.awt.BorderLayout;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.net.URI;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;

/**
 * Play Java4K applets without using a browser, and (when using an embedded JRE) without needing to have Java installed.
 */
@SuppressWarnings("serial")
public class Java4KWrapperApp extends JFrame {

	public static final int MAX_APPLET_WIDTH = 800;
	public static final int MAX_APPLET_HEIGHT = 665;

	private static final int ICON_IMAGE_WIDTH = 16;
	private static final int ICON_IMAGE_HEIGHT = 16;
	private static final int ICON_BITS_PER_PIXEL = 4;
	private static final int ICON_PIXELS_PER_VALUE = 4;

	private static final int[][] iconPalette = new int[][] { { 188, 0, 188 }, { 64, 64, 64 }, { 0, 0, 0 },
			{ 128, 128, 128 }, { 0, 135, 24 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 },
			{ 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 } };
	private static final String iconString = "\u1100\u1111\u1111\u0011\u2220\u2222\u2222\u0222\u2223\u2222\u2222\u1222\u2223\u2222\u2222\u1222\u2223\u4242\u4244\u1244\u2223\u2244\u2224\u1224\u4223\u2242\u4224\u1222\u2423\u2242\u2444\u1222\u4423\u2444\u4224\u1222\u2223\u2242\u2224\u1224\u2223\u4444\u2244\u1244\u2223\u2222\u2222\u1222\u2223\u2222\u2222\u1222\u2223\u2222\u2222\u1222\u2220\u2222\u2222\u0222\u1100\u1111\u1111\u0011";

	public static final String GENERAL_HELP = "Select one of the games from the dropbox to start playing.";
	public static final String LINK_ADDRESS = "http://www.grunntgames.com";

	public static final String[] appletNames = { "Farmer John and the Birds 4K", "Galactic Conquest 4K II", "Wizzy 4K" };
	public static final Class<?>[] appletClasses = { F.class, G.class, W.class };
	public static final String[] appletInstructions = {
			"Farmer John wakes up to find a swarm of nasty birds coming to eat all his precious corn. Help him kill them all before it�s too late!\n\nClick anywhere on the screen to fire John�s gun at that location. The birds will try to avoid you, so you will have to move quickly. Each level brings more and faster birds.\n\nThere�s different bird types that require slightly different approaches to kill. Sometimes the birds drop an egg. Shoot the purple eggs to get 10% reduction in reload time, shoot the yellow eggs to get extra ammo slots.",
			"The year is 4096 and all hell just broke loose. Xenophobia got the upper hand, and now all races in the known galaxy are at war. Cruisers are cruising, lasers are blasting and aliens are screaming. There is no surrender, only extermination or victory!\n\n- Left click = select single star (with nothing selected) or move ships from selected star(s) to this star.\n- Drag with left mouse button down = select multiple stars.\n- Right click = change number of ships to move from each selected star (all, half, one)\n- Mouse over an owned star to see its current infrastructure and build project (ship or starbase).\n- C key = toggle mouseover star as collection point. New ships move to collection points automagically.\n- S key = build star base at mouseover star.\n- H key = toggle visibility of balance of powers history graph.",
			"Wizzy, like most students, didn�t pay attention in the boring dark magic classes. Which is a pity, because one of his fellow students did. And he took over the University, turning all teachers and students into green slimy monsters! (while Wizzy was taking a nap) Not knowing any offensive spells, Wizzy has no chance of beating the Evil Guy. Not that he wants to anyway, he just wants to get out! The only spells Wizzy knows are gravity inversion spells, which he thought were rather neat. Help get out alive using these spells!\n\nInstructions: reach the door out of each level room while avoiding the monsters and spikes.\nPress space to start the game.\nMove and jump using the arrow keys.\nZ = cast inverse gravity for self spell,\nX = cast inverse gravity for others spell.\nTo cast a spell you one mana crystal, which you need to collect (the blue balls, collected crustals are shown in the bottom-right).\nPress R to restart the level." };

	private Applet currentApplet;

	// Override applet isActive methods.

	public static void main(String[] args) {
		Java4KWrapperApp mainFrame = new Java4KWrapperApp();
		mainFrame.run();
	}

	public void run() {

		// Convert string to sprite sheet
		BufferedImage iconImage = new BufferedImage(ICON_IMAGE_WIDTH, ICON_IMAGE_HEIGHT, BufferedImage.TYPE_INT_ARGB);
		int inputPos = 0;
		for (int y = 0; y < ICON_IMAGE_HEIGHT; y++) {
			for (int x = 0; x < ICON_IMAGE_WIDTH; x++) {
				int stringPos = inputPos / ICON_PIXELS_PER_VALUE;
				int partPos = inputPos % ICON_PIXELS_PER_VALUE;
				int paletteIndex = (iconString.charAt(stringPos) >>> (partPos * ICON_BITS_PER_PIXEL)) & 0x0F;
				int val = 0xff000000 | (iconPalette[paletteIndex][0] << 16) | (iconPalette[paletteIndex][1] << 8)
						| iconPalette[paletteIndex][2];
				// Transparency
				if (paletteIndex == 0)
					val = 0;
				iconImage.setRGB(x, y, val);
				inputPos++;
			}
		}
		setIconImage(iconImage);

		try {

			// Default look & feel is ugly, so use Nimbus if possible
			try {
				for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
					if ("Nimbus".equals(info.getName())) {
						UIManager.setLookAndFeel(info.getClassName());
						break;
					}
				}
			} catch (Exception e) {
				// If Nimbus is not available, you can set the GUI to another look and feel.
			}

			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			getContentPane().setLayout(new BorderLayout());

			final JPanel appletPanel = new JPanel();
			appletPanel.setLayout(new BorderLayout());
			getContentPane().add(appletPanel, BorderLayout.CENTER);

			JPanel controlPanel = new JPanel();
			controlPanel.setLayout(new FlowLayout(5));
			getContentPane().add(controlPanel, BorderLayout.NORTH);

			final JComboBox<String> appletSelector = new JComboBox<String>();
			for (String name : appletNames) {
				appletSelector.addItem(name);
			}
			appletSelector.addItem("Please select a game...");
			appletSelector.setSelectedIndex(appletClasses.length);
			appletSelector.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					try {
						if (currentApplet != null) {
							currentApplet.stop();
						}
						appletPanel.removeAll();

						// Only activate a new applet when one is selected
						if (appletSelector.getSelectedIndex() < appletClasses.length) {
							Class<?> appletClass = (Class<?>) appletClasses[appletSelector.getSelectedIndex()];
							Applet applet = (Applet) appletClass.newInstance();
							applet.setLayout(new BorderLayout());
							applet.setPreferredSize(new Dimension(800, 600));
							applet.setStub(new WrapperAppletStub());
							applet.init();
							appletPanel.add(applet, BorderLayout.CENTER);
							// TODO: set input focus to applet after loading?

							currentApplet = applet;
							currentApplet.start();
						}
						pack();
					} catch (Exception ex) {
						JOptionPane.showMessageDialog(null, ex.toString(), "Error", JOptionPane.ERROR_MESSAGE);
						ex.printStackTrace();
					}
				}
			});
			controlPanel.add(appletSelector);

			JButton helpButton = new JButton("?");
			helpButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					JDialog infoDialog = new JDialog(Java4KWrapperApp.this);
					JTextArea textArea = new JTextArea();
					textArea.setEditable(false);
					textArea.setLineWrap(true);
					textArea.setWrapStyleWord(true);
					infoDialog.setTitle("Instructions");
					infoDialog.setBounds(5, 5, 500, 500);
					infoDialog.setLayout(new BorderLayout());
					infoDialog.add(textArea, BorderLayout.CENTER);
					if (appletSelector.getSelectedIndex() >= appletClasses.length) {
						textArea.setText(GENERAL_HELP);
					} else {
						textArea.setText(appletInstructions[appletSelector.getSelectedIndex()]);
					}
					infoDialog.setVisible(true);
				}
			});
			controlPanel.add(helpButton);

			JButton linkButton = new JButton("Visit www.grunntgames.com for tips & more games");
			linkButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
					if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
						try {
							desktop.browse(new URI(LINK_ADDRESS));
						} catch (Exception e2) {
							JOptionPane.showMessageDialog(null, e2.toString(), "Error", JOptionPane.ERROR_MESSAGE);
							e2.printStackTrace();
						}
					}
				}
			});
			controlPanel.add(linkButton);

			setResizable(false);
			setBounds(50, 50, 100, 50);
			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			setTitle("Java4K games standalone starter - by GrunntGames");
			pack();
			setVisible(true);

		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e.toString(), "Error", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
	}
}
