



import java.applet.AppletContext;
import java.applet.AppletStub;
import java.net.URL;

public class WrapperAppletStub implements AppletStub {

	@Override
	public void appletResize(int width, int height) {
		// Do nothing
	}

	@Override
	public AppletContext getAppletContext() {
		return null;
	}

	@Override
	public URL getCodeBase() {
		return null;
	}

	@Override
	public URL getDocumentBase() {
		return null;
	}

	@Override
	public String getParameter(String name) {
		return null;
	}

	@Override
	public boolean isActive() {
		return true;
	}
}
