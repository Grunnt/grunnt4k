package utils;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

public class PngToByteArray {

	private int BITS_PER_PIXEL = 4;

	private List<Color> palette = new ArrayList<Color>();

	public void run(String fileName) throws Exception {
		BufferedImage img = ImageIO.read(new File(fileName));
		int[] rgb = new int[img.getWidth() * img.getHeight()];
		img.getRGB(0, 0, img.getWidth(), img.getHeight(), rgb, 0, img.getWidth());
		int[] pixelData = new int[img.getWidth() * img.getHeight()];
		for (int x = 0; x < img.getWidth(); x++) {
			for (int y = 0; y < img.getHeight(); y++) {
				// Get color values of this pixel
				int index = y * img.getWidth() + x;
				int R = (rgb[index] >> 16) & 0xff; // bitwise shifting
				int G = (rgb[index] >> 8) & 0xff;
				int B = rgb[index] & 0xff;

				// Do we already have this color?
				int paletteIndex = palette.size();
				for (int p = 0; p < palette.size(); p++) {
					Color color = palette.get(p);
					if (color.getRed() == R && color.getGreen() == G && color.getBlue() == B) {
						// Existing color, use it
						paletteIndex = p;
						break;
					}
				}

				if (paletteIndex == palette.size()) {
					// New color, add it
					palette.add(new Color(R, G, B));

					if (palette.size() > (2 ^ BITS_PER_PIXEL)) {
						throw new Exception("Too many colors for " + BITS_PER_PIXEL + "bit palette");
					}
				}

				// Remember this pixel's palette index
				pixelData[index] = paletteIndex;
			}
		}

		System.out.println("Color palette:");
		System.out.print("new Color[]{");
		boolean comma = false;
		for (Color color : palette) {
			if (!comma) {
				comma = true;
			} else {
				System.out.print(",");
			}
			System.out.print("new Color(" + color.getRed() + "," + color.getGreen() + "," + color.getBlue() + ")");
		}
		System.out.println("};");
		int valuesPerInt = 32 / BITS_PER_PIXEL;
		System.out.println("Pixel value array (" + (pixelData.length / valuesPerInt) + " values):");
		System.out.print("new int[]{");
		comma = false;
		for (int p = 0; p < pixelData.length / valuesPerInt; p++) {
			if (!comma) {
				comma = true;
			} else {
				System.out.print(",");
			}
			// Combine values
			int result = 0;
			for (int v = 0; v < valuesPerInt; v++) {
				int value = pixelData[p * valuesPerInt + v];
				result = result | (value << (BITS_PER_PIXEL * v));
			}

			// Print result
			System.out.print(result);
		}
		System.out.println("};");
	}

	public static void main(String[] args) {
		try {
			PngToByteArray converter = new PngToByteArray();
			converter.run("test.png");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
