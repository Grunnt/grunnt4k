package utils;

public class BitTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		for (int i = 0; i < 10; i++) {
			System.out.println(String.format("%16s", Integer.toBinaryString(i)).replace(' ', '0'));
		}
	}
}
